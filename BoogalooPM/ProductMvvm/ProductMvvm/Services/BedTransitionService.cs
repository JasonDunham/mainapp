﻿using MvvmFoundation.Wpf;
using ProductMvvm.Configuration;
using ProductMvvm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ProductMvvm.Services
{
    class BedTransitionService
    {
        //data checks and status indicators done in another class
        private readonly ProductDisplayModelStatus stat = new ProductDisplayModelStatus();
        public ProductDisplayModelStatus Stat { get { return stat; } }

        public bool TransitionUnavailableToAvailable(ViewModels.Bed bed, int checkinCode = 0)
        {
            Messenger messenger = App.Messenger;
            //check to confirm that a reservation already exists. If it does not, we can't transition from Unavailable
            ViewModels.Reservation res = App.BoogalooDB.GetCurrentReservationsByBedId(bed._BedId).FirstOrDefault();
            if (res == default(ViewModels.Reservation))
            {
                return false;
            }

            //check to see if the bed was marked as unavailable. If so, we can reverse this easily
            if (res.Disabled == true)
            {
                return DeleteReservation(res);
            }

            //if we get here, we now know we need to validate the checkout code to see if the bed can be returned properly. If it can,
            //then we can look at whether it was a return or a valid purchase.
            if (!ValidateCheckinCode(bed, checkinCode))
            {
                //MessageBox.Show("Invalid Checkin Code. Cannot check in bed.");

                messenger.NotifyColleagues("CipherFailedDialog");

                return false;
            }


            //at this point we know bed was checked out properly. Let's check to see if it had been used for less than the trial threshold
            //if so, we want to recall the old entry.
            //TODO add checkout code here too (for trial)
            TimeSpan elapsedTime = App.TimeManager.GetDateTime() - res.StartDate;
            if (elapsedTime.TotalHours <= Constants.TrailPeriodAllowance)
            {
                res.Recalled = true;
                res.EndDate = App.TimeManager.GetDateTime();
                messenger.NotifyColleagues("ShowCheckinReturnDialog");
                UpdateReservation(res);
                SelectAvailableBed(bed);
                return true;
            }

            //bed was returned after the trial period, thus require proper checkin code.
            //ADD HERE

            //MessageBox.Show("Need to Implement Checkin Code Functionality");

            messenger.NotifyColleagues("ShowCheckinChargedDialog");
            res.EndDate = App.TimeManager.GetDateTime();
            UpdateReservation(res);
            SelectAvailableBed(bed);
            return true;
        }

        public bool TransitionAvailableToUnavailable(ViewModels.Bed bed, string transitionType)
        {
            Messenger messenger = App.Messenger;
            ViewModels.Reservation currentRes = App.BoogalooDB.GetCurrentReservationsByBedId(bed._BedId).FirstOrDefault();

            //confirm that the bed is not already reserved
            if (currentRes != default(ViewModels.Reservation))
            { return false; }

            //check to see if we are marking the bed as unavailable
            if (transitionType == "Unavailable")
            {
                ViewModels.Reservation res = new ViewModels.Reservation(-1, bed._BedId, App.TimeManager.GetDateTime(), null, null, 1, true);
                return AddReservation(res);
            }

            //check to see if we are marking the bed as checked out
            if (transitionType == "Checkout")
            {
                ViewModels.Reservation res = new ViewModels.Reservation(-1, bed._BedId, App.TimeManager.GetDateTime(), null, null, 1, false);

                string cipher = Cipher.Generate(res.BedId,App.TimeManager.GetDateTime(),false);
                //MessageBox.Show("Please use this code to unlock bed " + bed.BedName + " : " + cipher);
                messenger.NotifyColleagues("ShowCheckoutCodeDialog", cipher);

                return AddReservation(res);
            }

            //something went wrong. Fail
            return false;

        }


        private bool DeleteReservation(ViewModels.Reservation res)
        {
            if (!App.BoogalooDB.DeleteReservation(res._ReservationId))
            {
                stat.Status = App.StoreDB.errorMessage;
                return false;
            }

            return true;
        }

        private bool AddReservation(ViewModels.Reservation res)
        {
            if (!App.BoogalooDB.AddReservation(res))
            {
                stat.Status = App.StoreDB.errorMessage;
                return false;
            }

            return true;
        }

        private bool UpdateReservation(ViewModels.Reservation res)
        {
            if (!App.BoogalooDB.UpdateReservation(res))
            {
                stat.Status = App.StoreDB.errorMessage;
                return false;
            }

            return true;
        }

        //assuming that a code is generated every minute, and codes are valid for the previous 30 minutes.
        private bool ValidateCheckinCode(ViewModels.Bed bed, int checkinCode)
        {
            for (int i=0; i<=30; i++)
            {
                DateTime time = App.TimeManager.GetDateTime().AddMinutes(-i);
                if (Int64.Parse(Cipher.Generate(bed._BedId, time, true)) == checkinCode)
                {
                    return true;
                }
            }
            return false;
        }

        private void SelectAvailableBed(ViewModels.Bed bed)
        {
            App.Messenger.NotifyColleagues("SelectAvailableBed", bed);
        }
    }
}
