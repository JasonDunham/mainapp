﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductMvvm.Services
{
    public class TimeManager {

        private DateTime currentTime;
        public DateTime CurrentTime {
            get { return currentTime; }
            set { currentTime = value; }
        }

        private bool realdate;
        public bool RealDate
        {
            get { return realdate; }
            set { realdate = value; }
        }

        public TimeManager()
        {
            currentTime = new DateTime(2014, 10, 31);
            realdate = true;
        }

        public DateTime GetDate()
        {
            if (realdate == true)
            {
                return DateTime.Now.Date;
            }
            return currentTime.Date;
        }

        public DateTime GetDateTime()
        {
            if (realdate == true)
            {
                return DateTime.Now;
            }
            return currentTime;
        }
    }
}
