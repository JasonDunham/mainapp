﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductMvvm.Services
{
    static class Cipher
    {
        public static string Generate(int bedId, DateTime time, bool checkoutMode, bool dumbMode = true, bool maintenance=false)
        {
            if (dumbMode == false)
            {
                return GetDumbCode(checkoutMode);
            }
            
            string BUNK_ID = "CAN-519-123";
            //return "150283";
            string outCode;

            uint a = Convert.ToUInt32(time.Minute);
            uint b = Convert.ToUInt32(time.Day);
            uint c = Convert.ToUInt32(time.Year);
            uint d = Convert.ToUInt32(time.Hour);
            uint h = Convert.ToUInt32(time.Month);

            uint e = (uint)(BUNK_ID.ElementAt(0) ^ BUNK_ID.ElementAt(1) ^ BUNK_ID.ElementAt(2)); // bunk id
            uint f = (uint)(BUNK_ID.ElementAt(4) ^ BUNK_ID.ElementAt(5) ^ BUNK_ID.ElementAt(6));// bunk id
            uint g = (uint)(BUNK_ID.ElementAt(8) ^ BUNK_ID.ElementAt(9) ^ BUNK_ID.ElementAt(10));// bunk id

            uint i = 0;
            uint j = 0;
            uint k = 0;
            uint l = 0;

            //if (maintenance)
            //{
            //    a = DateTime.Now.Minute;
            //    b = BUNK_ID[8] ^ BUNK_ID[9] ^ BUNK_ID[10];
            //    c = BUNK_ID[0] ^ BUNK_ID[1] ^ BUNK_ID[2];
            //    d = DateTime.Now.Hour;
            //    e = DateTime.Now.Year;
            //    f = DateTime.Now.Day;
            //    g = DateTime.Now.Month;
            //    h = BUNK_ID[4] ^ BUNK_ID[5] ^ BUNK_ID[6];
            //}

            if (checkoutMode == true)
            { // Checkout mode
                a = Convert.ToUInt32(time.Minute);
                b = (uint)(BUNK_ID.ElementAt(0) ^ BUNK_ID.ElementAt(1) ^ BUNK_ID.ElementAt(2));
                c = Convert.ToUInt32(time.Year);
                d = (uint)(BUNK_ID.ElementAt(4) ^ BUNK_ID.ElementAt(5) ^ BUNK_ID.ElementAt(6));
                e = Convert.ToUInt32(time.Day);
                f = (uint)(BUNK_ID.ElementAt(8) ^ BUNK_ID.ElementAt(9) ^ BUNK_ID.ElementAt(10));
                g = Convert.ToUInt32(time.Hour);
                h = Convert.ToUInt32(time.Month);
            }

            if (a % 30 == 0)
            {
                i = AppendNum(c, d, e, g);
                j = AppendNum(c, d, a, b);
                k = AppendNum(a, b, d, h);
                l = AppendNum(c, e, f, e);
            }
            else if (a % 15 == 0)
            {
                i = AppendNum(d, e, c, b);
                j = AppendNum(b, g, e, c);
                k = AppendNum(a, a, c, b);
                l = AppendNum(d, a, e, d);
            }
            else if (a % 40 == 0)
            {
                i = AppendNum(d, e, a, d);
                j = AppendNum(a, b ^ c, f, e);
                k = AppendNum(e, f, a, g);
                l = AppendNum(g, e, a, d);
            }
            else if (a % 20 == 0)
            {
                i = AppendNum(h, e, a ^ c, d);
                j = AppendNum(e, e, d, a);
                k = AppendNum(g, e, c, a);
                l = AppendNum(b, b, a, c);
            }
            else if (a % 25 == 0)
            {
                i = AppendNum(b, a, c, k);
                j = AppendNum(a, c, d, e);
                k = AppendNum(e, f ^ c, e, d);
                l = AppendNum(f, e, e, d);
            }
            else if (a % 32 == 0)
            {
                i = AppendNum(e, d, a, c);
                j = AppendNum(g, e, a, c);
                k = AppendNum(d, e, f, h);
                l = AppendNum(i, a, e ^ c, d);
            }
            else if (a % 24 == 0)
            {
                i = AppendNum(k, e, a, l);
                j = AppendNum(e, l, a, e);
                k = AppendNum(e, d, a, g);
                l = AppendNum(g, e ^ c, d, a);
            }
            else if (a % 26 == 0)
            {
                i = AppendNum(a, i, e, g);
                j = AppendNum(c, b, e, a);
                k = AppendNum(b, b ^ c, a, g);
                l = AppendNum(g, a, e, d);
            }
            else if (a % 12 == 0)
            {
                i = AppendNum(f, f, e, d);
                j = AppendNum(g, a, f, f);
                k = AppendNum(f, a, e, d);
                l = AppendNum(d, e, f ^ c, f);
            }
            else if (a % 18 == 0)
            {
                i = AppendNum(e, d, e, a);
                j = AppendNum(b, b, c, a);
                k = AppendNum(b, b, c, f);
                l = AppendNum(f ^ c, g, a, d);
            }
            else if (a % 6 == 0)
            {
                i = AppendNum(a, f, e, d);
                j = AppendNum(a, g, e, g);
                k = AppendNum(g, e, g, g);
                l = AppendNum(e, a, a, g);
            }
            else if (a % 9 == 0)
            {
                i = AppendNum(a, e, e ^ g, e);
                j = AppendNum(a, e, g, c);
                k = AppendNum(e, c, e, f);
                l = AppendNum(e, g ^ c, c, a);
            }
            else if (a % 8 == 0)
            {
                i = AppendNum(b, a, e, c);
                j = AppendNum(c, e, b, a);
                k = AppendNum(e, b, f, f);
                l = AppendNum(g ^ c, g, g, e);
            }
            else if (a % 4 == 0)
            {
                i = AppendNum(e, a, f, e);
                j = AppendNum(e, g, e, c);
                k = AppendNum(e, e, g, e);
                l = AppendNum(e, a, a ^ c, b);
            }
            else if (a % 17 == 0)
            {
                i = AppendNum(b, a, e, e);
                j = AppendNum(g, e, f, f);
                k = AppendNum(g, e, d, a);
                l = AppendNum(a, e, e ^ c, d);
            }
            else if (a % 23 == 0)
            {
                i = AppendNum(a, e, e, e);
                j = AppendNum(l, i, f, e);
                k = AppendNum(g, e, a, h);
                l = AppendNum(g, e, e, a);
            }
            else if (a % 11 == 0)
            {
                i = AppendNum(f, e, a, d);
                j = AppendNum(a, a ^ c, a, c);
                k = AppendNum(e, c, e, f);
                l = AppendNum(g, e, f, f);
            }
            else if (a % 13 == 0)
            {
                i = AppendNum(f, a, a, a);
                j = AppendNum(b, b, e, a);
                k = AppendNum(b, e, d, a);
                l = AppendNum(b, e, d, d);
            }
            else if (a % 14 == 0)
            {
                i = AppendNum(e, a, d, d);
                j = AppendNum(a, i, e, e);
                k = AppendNum(g, g, e, a);
                l = AppendNum(c, c, e, b);
            }
            else if (a % 7 == 0)
            {
                i = AppendNum(b, e, d, f);
                j = AppendNum(h, e, f, a);
                k = AppendNum(g, e, h, d);
                l = AppendNum(e, e, d, a);
            }
            else if (a % 3 == 0)
            {
                i = AppendNum(c, c, e, a);
                j = AppendNum(c, c, e, h);
                k = AppendNum(a, h, c, e);
                l = AppendNum(a, c, e, f);
            }
            else if (a % 5 == 0)
            {
                i = AppendNum(a, c, d, e);
                j = AppendNum(e, c, c, a);
                k = AppendNum(c, e, a, a);
                l = AppendNum(c, e, h, h);
            }
            else if (a % 2 == 0)
            {
                i = AppendNum(a, e, c, c);
                j = AppendNum(a, g ^ c, g, e);
                k = AppendNum(b, e, g, g);
                l = AppendNum(h, f, e, e);
            }
            else
            { // (minute % 1 == 0
                i = AppendNum(h, e, f, f);
                j = AppendNum(g, e, d, g);
                k = AppendNum(h, e, d, a);
                l = AppendNum(g, e ^ c, d, c);
            }

            uint value = i ^ j ^ k ^ l;
            uint temp = value / 10 * 10;

            string[] uberTempStrings = new string[6];

            for (int banana = 5; banana >= 0; banana--)
            {
                uberTempStrings[banana] = Convert.ToChar('0' + (value - value / 10 * 10)).ToString();
                value = (value / 10);
            }
            outCode = uberTempStrings[0] + uberTempStrings[1] + uberTempStrings[2] + uberTempStrings[3] + uberTempStrings[4] + uberTempStrings[5];

            return outCode;
        }

        //APPENDS NUMBERS TOGETHER INTO A UINT 32 BY SHIFTING THEM LEFT; USED IN CIPHER
        public static uint AppendNum(uint w, uint x, uint y, uint z)
        {
            return ((((((w << 8) ^ x) << 8) ^ y) << 8) ^ z);
        }

        public static string GetDumbCode(bool checkoutMode)
        {
            if (checkoutMode == true)
                return "412516";
            return "921902";
        }
    }
}
