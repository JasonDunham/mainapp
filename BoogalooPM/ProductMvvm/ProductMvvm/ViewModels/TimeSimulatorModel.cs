﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MvvmFoundation.Wpf;
using ProductMvvm.Services;

namespace ProductMvvm.ViewModels
{
    public class TimeSimulatorModel : INotifyPropertyChanged
    {

        private DateTime currentTime; 
        
        public TimeSimulatorModel()
        {
            CurrentTime = App.TimeManager.GetDateTime();
        }

        private bool realTime = true;
        public bool RealTime
        {
            get { return realTime; }
            set
            {
                realTime = value;
                OnPropertyChanged(new PropertyChangedEventArgs("RealTime"));
                App.TimeManager.RealDate = realTime;
            }
        }

        public DateTime CurrentTime
        {
            get { return currentTime; }
            set { 
                currentTime = value; 
                OnPropertyChanged(new PropertyChangedEventArgs("CurrentTime"));
                App.TimeManager.CurrentTime = currentTime;
            }
        }

        private string checkinCode = "0";
        public string CheckinCode
        {
            get { return checkinCode; }
            set
            {
                checkinCode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CheckinCode"));
                App.TimeManager.CurrentTime = currentTime;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }


        private RelayCommand listBoxCommand;
        public ICommand ListBoxCommand
        {
            get { return listBoxCommand; }
        }

        private RelayCommand continueCommand;
        public ICommand ContinueCommand
        {
            get { return continueCommand ?? (continueCommand = new RelayCommand(() => Continue())); }
        }

        private void Continue()
        {
            DateTime now = App.TimeManager.GetDateTime();
            CheckinCode = Cipher.Generate(7, now, true);
            //OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

    }//class ProductSelectionModel

}
