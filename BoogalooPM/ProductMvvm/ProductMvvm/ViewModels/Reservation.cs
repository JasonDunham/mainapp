﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ProductMvvm.ViewModels
{
    //Class for the GUI to display and modify products.
    //All product properties the GUI can touch are strings.
    //A single integer property, ProductId, is for database use only.
    //It is assigned by the DB when it creates a new product.  It is used
    //to identify a product and must not be modified by the GUI.
    public class Reservation
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        //For DB use only!
        private int _reservationId;
        public int _ReservationId { get { return _reservationId; } }

        private int bedId;
        public int BedId
        {
            get { return bedId; }
            set { bedId = value; OnPropertyChanged(new PropertyChangedEventArgs("BedId"));
                }
        }


        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; OnPropertyChanged(new PropertyChangedEventArgs("StartDate")); }
        }

        private DateTime? endDate;
        public DateTime? EndDate
        {
            get { return endDate; }
            set { endDate = value; OnPropertyChanged(new PropertyChangedEventArgs("EndDate")); }
        }

        private bool? recalled;
        public bool? Recalled
        {
            get { return recalled; }
            set { recalled = value; OnPropertyChanged(new PropertyChangedEventArgs("Recalled")); }
        }

        private bool? disabled;
        public bool? Disabled
        {
            get { return disabled; }
            set { disabled = value; OnPropertyChanged(new PropertyChangedEventArgs("Recalled")); }
        }

        private int employeeId;
        public int EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; OnPropertyChanged(new PropertyChangedEventArgs("CategoryName")); }
        }

        public Reservation()
        {
        }

        public Reservation(int reservationId, int bedId, DateTime startDate,
                       DateTime? endDate, bool? recalled, int employeeId, bool? disabled)
        {
            this._reservationId = reservationId;
            BedId = bedId;
            StartDate = startDate;
            EndDate = endDate;
            Recalled = recalled;
            EmployeeId = employeeId;
            Disabled = disabled;
        }

        public void CopyReservation(Reservation p)
        {
            this._reservationId = p._ReservationId;
            this.BedId = p.BedId;
            this.StartDate = p.StartDate;
            this.EndDate = p.EndDate;
            this.EmployeeId = p.EmployeeId;
            this.Recalled = p.Recalled;
            this.Disabled = p.Disabled;
        }

        //Creating a new product in the DB assigns the ProductId
        //Update the ProductId from the value in the corresponding SqlProduct
        public void ReservationAdded2DB(SqlReservation sqlReservation)
        {
            this._reservationId = sqlReservation.ReservationId;
        }

    } //class Product



    //Communiction to/from SQL uses this class for product
    //It has a decimal, not string, definition for UnitCost
    //Consversion routines SqlProduct <--> Product provided
    public class SqlReservation
    {
        public int ReservationId { get; set; }
        public int BedId {get; set;}
        public DateTime StartDate {get; set;}
        public DateTime? EndDate {get; set;}
        public bool? Recalled {get; set;}
        public int EmployeeId {get; set;}
        public bool? Disabled { get; set; }

        public SqlReservation() { }

        public SqlReservation(int reservationId, int bedId, DateTime startDate,
                       DateTime? endDate, bool? recalled, int employeeId, bool? disabled)
        {
            ReservationId = reservationId;
            BedId = bedId;
            StartDate = startDate;
            EndDate = endDate;
            Recalled = recalled;
            EmployeeId = employeeId;
            Disabled = disabled;
        }

        public SqlReservation(Reservation p)
        {
            ReservationId = p._ReservationId;
            BedId = p.BedId;
            StartDate = p.StartDate;
            EndDate = p.EndDate;
            Recalled = p.Recalled;
            EmployeeId = p.EmployeeId;
            Disabled = p.Disabled;
        }

        public Reservation SqlReservation2Reservation()
        {
            string unitCost = EndDate.ToString();
            return new Reservation(ReservationId, BedId, StartDate, EndDate, Recalled, EmployeeId, Disabled);
        } //SqlProduct2Product()
    }

}
