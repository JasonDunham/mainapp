﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MvvmFoundation.Wpf;

namespace ProductMvvm.ViewModels
{
    public class RoomSelectionModel : INotifyPropertyChanged
    {

        public RoomSelectionModel()
        {
            dataItems = new MyObservableCollection2<Room>();
            DataItems = App.BoogalooDB.GetRooms();
            listBoxCommand = new RelayCommand(() => SelectionHasChanged());
            App.Messenger.Register("RoomCleared", (Action)(() => SelectedRoom = null));
            App.Messenger.Register("GetRooms", (Action)(() => GetRooms()));
            App.Messenger.Register("UpdateRoom", (Action<Room>)(param => UpdateRoom(param)));
            App.Messenger.Register("DeleteRoom", (Action)(() => DeleteRoom()));
            App.Messenger.Register("AddRoom", (Action<Room>)(param => AddRoom(param)));
        }


        private void GetRooms()
        {
            DataItems = App.BoogalooDB.GetRooms();
            if (App.StoreDB.hasError)
                App.Messenger.NotifyColleagues("SetStatus", App.StoreDB.errorMessage);
        }


        private void AddRoom(Room p)
        {
            dataItems.Add(p);
        }


        private void UpdateRoom(Room p)
        {
            int index = dataItems.IndexOf(selectedRoom);
            dataItems.ReplaceItem(index, p);
            SelectedRoom = p;
        }


        private void DeleteRoom()
        {
            dataItems.Remove(selectedRoom);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private MyObservableCollection2<Room> dataItems;
        public MyObservableCollection2<Room> DataItems
        {
            get { return dataItems; }
            //If dataItems replaced by new collection, WPF must be told
            set { dataItems = value; OnPropertyChanged(new PropertyChangedEventArgs("DataItems")); }
        }

        private Room selectedRoom;
        public Room SelectedRoom
        {
            get { return selectedRoom; }
            set { selectedRoom = value; OnPropertyChanged(new PropertyChangedEventArgs("SelectedProduct")); }
        }

        private RelayCommand listBoxCommand;
        public ICommand ListBoxCommand
        {
            get { return listBoxCommand; }
        }

        private void SelectionHasChanged()
        {
            Messenger messenger = App.Messenger;
            messenger.NotifyColleagues("RoomSelectionChanged", selectedRoom);
        }
    }//class ProductSelectionModel



    public class MyObservableCollection2<Room> : ObservableCollection<Room>
    {
        public void UpdateCollection()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                                NotifyCollectionChangedAction.Reset));
        }


        public void ReplaceItem(int index, Room item)
        {
            base.SetItem(index, item);
        }

    } // class MyObservableCollection2
}
