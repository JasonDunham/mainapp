﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmFoundation.Wpf;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using ProductMvvm.Services;



namespace ProductMvvm.ViewModels
{
    public class BunkInformationModel : INotifyPropertyChanged
    {
        private bool isSelected = false;
        private BedTransitionService bTService;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
        //data checks and status indicators done in another class
        private readonly ProductDisplayModelStatus stat = new ProductDisplayModelStatus();
        public ProductDisplayModelStatus Stat { get { return stat; } }

        private Bed displayedBed = new Bed();
        public Bed DisplayedBed
        {
            get { return displayedBed; }
            set { 
                displayedBed = value;
                displayedBed.MostRecentReserve = App.BoogalooDB.GetMostRecentReservationsByBedId(displayedBed._BedId);
                SetBedProperties();
                OnPropertyChanged(new PropertyChangedEventArgs("DisplayedBed")); 
            }
        }

        private BedProperties bedProperties = new BedProperties();
        public BedProperties BedProperties
        {
            get { return bedProperties; }
            set { bedProperties = value; }
        }

        private RelayCommand getBedsCommand;
        public ICommand GetBedsCommand
        {
            get { return getBedsCommand ?? (getBedsCommand = new RelayCommand(() => GetBeds())); }
        }

        private void GetBeds()
        {
            isSelected = false;
            stat.NoError();
            DisplayedBed = new Bed();
            App.Messenger.NotifyColleagues("GetBeds");
        }


        private RelayCommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand ?? (clearCommand = new RelayCommand(() => ClearProductDisplay()/*, ()=>isSelected*/)); }
        }

        private void ClearProductDisplay()
        {
            isSelected = false;
            stat.NoError();
            DisplayedBed = new Bed();
            App.Messenger.NotifyColleagues("BedCleared");
        } //ClearProductDisplay()


        private RelayCommand update2Command;
        public ICommand Update2Command
        {
            get { return update2Command ?? (update2Command = new RelayCommand(() => UpdateBed()/*, ()=>isSelected*/)); }
        }

        private void UpdateBed()
        {
            //if (!stat.ChkProductForUpdate(DisplayedBed)) return;
                if(!App.BoogalooDB.UpdateBed(DisplayedBed))
                {
                    stat.Status = App.StoreDB.errorMessage;
                    return;
                }
                App.Messenger.NotifyColleagues("UpdateBed", DisplayedBed);
        } //UpdateProduct()


        private RelayCommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand ?? (deleteCommand = new RelayCommand(() => DeleteBed(), () => isSelected)); }
        }

        private RelayCommand bedUnavailableCommand;
        public ICommand BedUnavailableCommand
        {
            get { return bedUnavailableCommand ?? (bedUnavailableCommand = new RelayCommand(() => MarkUnavailableBed(), () => isSelected)); }
        }

        private RelayCommand bedAvailableCommand;
        public ICommand BedAvailableCommand
        {
            get { return bedAvailableCommand ?? (bedAvailableCommand = new RelayCommand(() => MarkAvailableBed(), () => isSelected)); }
        }

        private void MarkUnavailableBed()
        {
            bTService.TransitionAvailableToUnavailable(DisplayedBed,"Unavailable");
            isSelected = false;
            App.Messenger.NotifyColleagues("MarkUnavailableBed");
        } //DeleteProduct

        private void MarkAvailableBed()
        {
            bTService.TransitionUnavailableToAvailable(DisplayedBed);
            isSelected = false;
            App.Messenger.NotifyColleagues("MarkAvailableBed");

        } //DeleteProduct

        private void MarkCheckoutBed()
        {
            bTService.TransitionAvailableToUnavailable(DisplayedBed, "Checkout");
            isSelected = false;
            App.Messenger.NotifyColleagues("MarkUnavailableBed");
        }

        private void MarkCheckinBed(User displayUser)
        {
            int checkinCode = displayUser.CheckinCode;
            bTService.TransitionUnavailableToAvailable(DisplayedBed, checkinCode);
            isSelected = false;
            App.Messenger.NotifyColleagues("MarkAvailableBed");

        }

        private RelayCommand checkoutBedCommand;
        public ICommand CheckoutBedCommand
        {
            get { return checkoutBedCommand ?? (checkoutBedCommand = new RelayCommand(() => MarkCheckoutBed(), () => isSelected)); }
        }


        private void DeleteBed()
        {
            if (!App.BoogalooDB.DeleteBed(DisplayedBed._BedId))
            {
                stat.Status = App.StoreDB.errorMessage;
                return;
            }
            isSelected = false;
            App.Messenger.NotifyColleagues("DeleteBed");
        } //DeleteProduct


        private RelayCommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand ?? (addCommand = new RelayCommand(() => AddBed(), () => !isSelected)); }
        }


        private void AddBed()
        {
            //if (!stat.ChkProductForAdd(DisplayedBed)) return;
            if (!App.BoogalooDB.AddBed(DisplayedBed))
            {
                stat.Status = App.StoreDB.errorMessage;
                return;
            }
            App.Messenger.NotifyColleagues("AddBed", DisplayedBed);
        } //AddProduct()


        public BunkInformationModel()
        {
            bTService = new BedTransitionService();

            Messenger messenger = App.Messenger;
            messenger.Register("ActiveBunkSelectionChanged", (Action<Bed>)(param => ProcessBed(param)));
            messenger.Register("InactiveBunkSelectionChanged", (Action<Bed>)(param => ProcessBed(param)));
            messenger.Register("ReservedBunkSelectionChanged", (Action<Bed>)(param => ProcessBed(param)));
            messenger.Register("DisabledBunkSelectionChanged", (Action<Bed>)(param => ProcessBed(param)));
            messenger.Register("SetStatus", (Action<String>)(param => stat.Status = param));
            messenger.Register("CheckoutBed", (Action)(() => MarkCheckoutBed()));
            messenger.Register("CheckinBed", (Action<User>)(param => MarkCheckinBed(param)));
            messenger.Register("ActivateBed", (Action)(() => MarkAvailableBed()));
            messenger.Register("UnavailableBed", (Action)(() => MarkUnavailableBed()));
        } //ctor

        public void ProcessBed(Bed p)
        {
            if (p == null) { /*DisplayedProduct = null;*/  isSelected = false;  return; }
            Bed temp = new Bed();
            temp.CopyBed(p);
            DisplayedBed = temp;
            isSelected = true;
            stat.NoError();
        } // ProcessProduct()

        private void SetBedProperties()
        {
            bedProperties = new BedProperties();
            if (displayedBed.Status == 2) {
                bedProperties.BedStatus = Bed_Status_Enum.Disabled;
            }
            else if (displayedBed.MostRecentReserve == null)
            {
                bedProperties.BedStatus = Bed_Status_Enum.Available;
            }
            else if (displayedBed.MostRecentReserve.EndDate != null)
            {
                bedProperties.BedStatus = Bed_Status_Enum.Available;
                bedProperties.StatusElapse = (App.TimeManager.GetDateTime() - displayedBed.MostRecentReserve.StartDate);
            }
            else if (displayedBed.MostRecentReserve.Disabled == true)
            {
                bedProperties.BedStatus = Bed_Status_Enum.Unavailable;
                bedProperties.StatusElapse = (App.TimeManager.GetDateTime() - displayedBed.MostRecentReserve.StartDate);
            }
            else
            {
                bedProperties.BedStatus = Bed_Status_Enum.Booked;
                bedProperties.StatusElapse = (App.TimeManager.GetDateTime() - displayedBed.MostRecentReserve.StartDate);
            }
            OnPropertyChanged(new PropertyChangedEventArgs("BedProperties"));
        }
    }
}
