﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MvvmFoundation.Wpf;

namespace ProductMvvm.ViewModels
{
    public class InactiveBunkSelectionModel : INotifyPropertyChanged
    {

        public InactiveBunkSelectionModel()
        {
            dataItems = new MyObservableCollectionBunk<Bed>();
            GetInactiveBunks();
            listBoxCommand = new RelayCommand(() => SelectionHasChanged());
            App.Messenger.Register("BunkCleared", (Action)(() => SelectedBunk = null));
            App.Messenger.Register("GetBeds", (Action)(() => GetInactiveBunks()));
            App.Messenger.Register("UpdateBunk", (Action<Bed>)(param => UpdateBunk(param)));
            App.Messenger.Register("RoomSelectionChanged", (Action<Room>)(param => ChangeRoomSelection(param)));
            App.Messenger.Register("DeleteBunk", (Action)(() => DeleteBunk()));
            App.Messenger.Register("AddBunk", (Action<Bed>)(param => AddBunk(param)));
            App.Messenger.Register("MarkUnavailableBed", (Action)(() => GetInactiveBunks()));
            App.Messenger.Register("MarkAvailableBed", (Action)(() => GetInactiveBunks()));
        }

        private Room activeRoom = default(Room);

        private void ChangeRoomSelection(Room room)
        {
            activeRoom = room;
            GetInactiveBunks();
        }

        private void GetInactiveBunks()
        {
            MyObservableCollectionBunk<Bed> bunks;
            if(activeRoom==default(Room))
            {
                bunks = App.BoogalooDB.GetBeds();
            }
            else
            {
                bunks = App.BoogalooDB.GetBedsByRoomId(activeRoom);
            }
            MyObservableCollectionReservation<Reservation> todayReserves = App.BoogalooDB.GetAllCurrentReservations();
            MyObservableCollectionBunk<Bed> inactiveBunks = new MyObservableCollectionBunk<Bed>();

            foreach (Reservation res in todayReserves)
            {
                Bed tempBed = bunks.FirstOrDefault(t => t._BedId == res.BedId);
                if (tempBed != default(Bed))
                {
                    inactiveBunks.Add(tempBed);
                }
            }
            DataItems = inactiveBunks;
            if (App.StoreDB.hasError)
                App.Messenger.NotifyColleagues("SetStatus", App.StoreDB.errorMessage);
        }


        private void AddBunk(Bed p)
        {
            dataItems.Add(p);
        }


        private void UpdateBunk(Bed p)
        {
            int index = dataItems.IndexOf(selectedBunk);
            dataItems.ReplaceItem(index, p);
            SelectedBunk = p;
        }


        private void DeleteBunk()
        {
            dataItems.Remove(selectedBunk);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private MyObservableCollectionBunk<Bed> dataItems;
        public MyObservableCollectionBunk<Bed> DataItems
        {
            get { return dataItems; }
            //If dataItems replaced by new collection, WPF must be told
            set { dataItems = value; OnPropertyChanged(new PropertyChangedEventArgs("DataItems")); }
        }

        private Bed selectedBunk;
        public Bed SelectedBunk
        {
            get { return selectedBunk; }
            set { selectedBunk = value; OnPropertyChanged(new PropertyChangedEventArgs("SelectedBunk")); }
        }

        private RelayCommand listBoxCommand;
        public ICommand ListBoxCommand
        {
            get { return listBoxCommand; }
        }

        private void SelectionHasChanged()
        {
            if (selectedBunk == null) { return; }
            Messenger messenger = App.Messenger;
            MyObservableCollectionReservation<Reservation> reserves = App.BoogalooDB.GetCurrentReservationsByBedId(selectedBunk._BedId);
            if (reserves.First().Disabled == true)
            {
                messenger.NotifyColleagues("InactiveBunkSelectionChanged", selectedBunk);
                return;
            }
            messenger.NotifyColleagues("ReservedBunkSelectionChanged", selectedBunk);
        }
    }//class ProductSelectionModel


}
