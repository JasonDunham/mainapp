﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmFoundation.Wpf;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;



namespace ProductMvvm.ViewModels
{
    public class CheckoutBedModel : INotifyPropertyChanged
    {
        private bool isSelected = false;

        private void CheckoutVisible()
        {
            ClearUserDisplay();
            DisplayFlags.CheckoutVisible = true;
            DisplayFlags.CheckinVisible = false;
            DisplayFlags.DisabledVisible = false;
            DisplayFlags.ActivateVisible = false;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        private void CheckinVisible(Bed bed)
        {
            ClearUserDisplay();
            DisplayFlags.CheckoutVisible = false;
            DisplayFlags.CheckinVisible = true;
            DisplayFlags.DisabledVisible = false;
            DisplayFlags.ActivateVisible = false;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        private void ActivateVisible(Bed bed)
        {
            ClearUserDisplay();
            DisplayFlags.CheckoutVisible = false;
            DisplayFlags.CheckinVisible = false;
            DisplayFlags.DisabledVisible = false;
            DisplayFlags.ActivateVisible = true;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
        //data checks and status indicators done in another class
        private readonly ProductDisplayModelStatus stat = new ProductDisplayModelStatus();
        public ProductDisplayModelStatus Stat { get { return stat; } }

        private User displayedUser = new User();
        public User DisplayedUser
        {
            get { return displayedUser; }
            set { displayedUser = value; OnPropertyChanged(new PropertyChangedEventArgs("DisplayedUser")); }
        }

        private DisplayFlags displayFlags = new DisplayFlags();
        public DisplayFlags DisplayFlags
        {
            get { return displayFlags; }
            set { displayFlags = value; OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags")); }
        }

        private RelayCommand getProductsCommand; 
        public ICommand GetProductsCommand
        {
            get { return getProductsCommand ?? (getProductsCommand = new RelayCommand(() => GetUsers())); }
        }

        private RelayCommand checkoutVisibleCommand;
        public ICommand CheckoutVisibleCommand
        {
            get { return checkoutVisibleCommand ?? (checkoutVisibleCommand = new RelayCommand(() => CheckoutVisible())); }
        }

        private void GetUsers()
        {
            isSelected = false;
            stat.NoError();
            DisplayedUser = new User();
            App.Messenger.NotifyColleagues("GetUsers");
        }


        private RelayCommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand ?? (clearCommand = new RelayCommand(() => ClearUserDisplay()/*, ()=>isSelected*/)); }
        }

        private void ClearUserDisplay()
        {
            isSelected = false;
            stat.NoError();
            DisplayedUser = new User();
            App.Messenger.NotifyColleagues("UserCleared");
        } //ClearProductDisplay()


        private RelayCommand updateCommand;
        public ICommand UpdateCommand
        {
            get { return updateCommand ?? (updateCommand = new RelayCommand(() => UpdateProduct(), ()=>isSelected)); }
        }

        private void UpdateProduct()
        {
            //if (!stat.ChkProductForUpdate(DisplayedUser)) return;
                //if(!App.StoreDB.UpdateProduct(DisplayedUser))
                //{
                //    stat.Status = App.StoreDB.errorMessage;
                //    return;
                //}
                //App.Messenger.NotifyColleagues("UpdateProduct", DisplayedProduct);
        } //UpdateProduct()


        private RelayCommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand ?? (deleteCommand = new RelayCommand(() => DeleteUser(), () => isSelected)); }
        }


        private void DeleteUser()
        {
            //if (!App.StoreDB.DeleteProduct(DisplayedProduct._ProductId))
            //{
            //    stat.Status = App.StoreDB.errorMessage;
            //    return;
            //}
            //isSelected = false;
            //App.Messenger.NotifyColleagues("DeleteProduct");
        } //DeleteProduct


        private RelayCommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand ?? (addCommand = new RelayCommand(() => AddUser(), () => !isSelected)); }
        }

        private RelayCommand checkoutCommand;
        public ICommand CheckoutCommand
        {
            get { return checkoutCommand ?? (checkoutCommand = new RelayCommand(() => CheckoutBed(), () => !isSelected)); }
        }

        private void CheckoutBed()
        {
            App.Messenger.NotifyColleagues("ShowCheckoutDialog");
        }

        private RelayCommand checkinCommand;
        public ICommand CheckinCommand
        {
            get { return checkinCommand ?? (checkinCommand = new RelayCommand(() => CheckinBed(), () => !isSelected)); }
        }

        private void CheckinBed()
        {
            App.Messenger.NotifyColleagues("ShowCheckinDialog");
        }

        private RelayCommand markUnavailableCommand;
        public ICommand MarkUnavailableCommand
        {
            get { return markUnavailableCommand ?? (markUnavailableCommand = new RelayCommand(() => MarkUnavailable(), () => !isSelected)); }
        }

        private void MarkUnavailable()
        {
            App.Messenger.NotifyColleagues("ShowUnavailableDialog");
        }

        private RelayCommand activateCommand;
        public ICommand ActivateCommand
        {
            get { return activateCommand ?? (activateCommand = new RelayCommand(() => ActivateBed(), () => !isSelected)); }
        }

        private void ActivateBed()
        {
            App.Messenger.NotifyColleagues("ShowActivateDialog");
        }


        private void AddUser()
        {
            //if (!stat.ChkProductForAdd(DisplayedProduct)) return;
            //if (!App.StoreDB.AddProduct(DisplayedProduct))
            //{
            //    stat.Status = App.StoreDB.errorMessage;
            //    return;
            //}
            //App.Messenger.NotifyColleagues("AddProduct", DisplayedProduct);
        } //AddProduct()


        public CheckoutBedModel()
        { 
            Messenger messenger = App.Messenger;
            messenger.Register("ActiveBunkSelectionChanged", (Action<Bed>)(param => CheckoutVisible()));
            messenger.Register("InactiveBunkSelectionChanged", (Action<Bed>)(param => ActivateVisible(param)));
            messenger.Register("ReservedBunkSelectionChanged", (Action<Bed>)(param => CheckinVisible(param)));
            messenger.Register("DisabledBunkSelectionChanged", (Action<Bed>)(param => LoadDisabledBunk()));
            messenger.Register("SetStatus", (Action<String>)(param => stat.Status = param));
        } //ctor

        public void LoadDisabledBunk()
        {
            //ClearUserDisplay();
            //checkinVisible = false;
            //CheckoutVisible = false;
            //oooVisible = true;
        }

        public void ProcessUser(User p)
        {
            if (p == null) { /*DisplayedProduct = null;*/  isSelected = false;  return; }
            User temp = new User();
            temp.CopyUser(p);
            DisplayedUser = temp;
            isSelected = true;
            stat.NoError();
        } // ProcessProduct()
    }
}
