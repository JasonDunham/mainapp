﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductMvvm.ViewModels
{
    public enum Bed_Status_Enum { Available, Booked, Unavailable, Disabled };


    public class BedProperties
    {
        public Bed_Status_Enum BedStatus { get; set; }
        public TimeSpan StatusElapse { get; set; }

    }
}
