﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MvvmFoundation.Wpf;

namespace ProductMvvm.ViewModels
{
    public class BunkSelectionModel : INotifyPropertyChanged
    {

        public BunkSelectionModel()
        {
            dataItems = new MyObservableCollectionBunk<Bed>();
            GetBunks();
            listBoxCommand = new RelayCommand(() => SelectionHasChanged());
            App.Messenger.Register("BunkCleared", (Action)(() => SelectedBunk = null));
            App.Messenger.Register("GetBeds", (Action)(() => GetBunks()));
            App.Messenger.Register("UpdateBunk", (Action<Bed>)(param => UpdateBunk(param)));
            App.Messenger.Register("RoomSelectionChanged", (Action<Room>)(param => ChangeRoomSelection(param)));
            App.Messenger.Register("DeleteBunk", (Action)(() => DeleteBunk()));
            App.Messenger.Register("AddBunk", (Action<Bed>)(param => AddBunk(param)));
            App.Messenger.Register("MarkUnavailableBed", (Action)(() => GetBunks()));
            App.Messenger.Register("MarkAvailableBed", (Action)(() => GetBunks()));
            App.Messenger.Register("SelectAvailableBed", (Action<Bed>)(param => SelectAvailableBed(param)));
        }

        private Room activeRoom = default(Room);

        private void ChangeRoomSelection(Room room)
        {
            activeRoom = room;
            GetBunks();
        }

        private void GetBunks()
        {
            MyObservableCollectionBunk<Bed> bunks;
            if(activeRoom==default(Room))
            {
                bunks = App.BoogalooDB.GetBeds();
            }
            else
            {
                bunks = App.BoogalooDB.GetBedsByRoomId(activeRoom);
            }
            MyObservableCollectionReservation<Reservation> todayReserves = App.BoogalooDB.GetAllCurrentReservations();
            MyObservableCollectionBunk<Bed> availableBunks = new MyObservableCollectionBunk<Bed>();

            foreach (Bed bed in bunks)
            {
                if(todayReserves.FirstOrDefault(t => t.BedId == bed._BedId) == default(Reservation))
                {
                   availableBunks.Add(bed);                 
                }
            }
            DataItems = availableBunks;
            if (App.StoreDB.hasError)
                App.Messenger.NotifyColleagues("SetStatus", App.StoreDB.errorMessage);
        }

        private void SelectAvailableBed(Bed bed)
        {
            SelectedBunk = bed;
        }

        private void AddBunk(Bed p)
        {
            dataItems.Add(p);
        }


        private void UpdateBunk(Bed p)
        {
            int index = dataItems.IndexOf(selectedBunk);
            dataItems.ReplaceItem(index, p);
            SelectedBunk = p;
        }


        private void DeleteBunk()
        {
            dataItems.Remove(selectedBunk);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private MyObservableCollectionBunk<Bed> dataItems;
        public MyObservableCollectionBunk<Bed> DataItems
        {
            get { return dataItems; }
            //If dataItems replaced by new collection, WPF must be told
            set { dataItems = value; OnPropertyChanged(new PropertyChangedEventArgs("DataItems")); }
        }

        private Bed selectedBunk;
        public Bed SelectedBunk
        {
            get { return selectedBunk; }
            set { selectedBunk = value; OnPropertyChanged(new PropertyChangedEventArgs("SelectedBunk")); }
        }

        private RelayCommand listBoxCommand;
        public ICommand ListBoxCommand
        {
            get { return listBoxCommand; }
        }

        private void SelectionHasChanged()
        {
            Messenger messenger = App.Messenger;
            messenger.NotifyColleagues("ActiveBunkSelectionChanged", selectedBunk);
        }
    }//class ProductSelectionModel



    public class MyObservableCollectionBunk<Bed> : ObservableCollection<Bed>
    {
        public void UpdateCollection()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                                NotifyCollectionChangedAction.Reset));
        }

         
        public void ReplaceItem(int index, Bed item)
        {
            base.SetItem(index, item);
        }

    } // class MyObservableCollection
}
