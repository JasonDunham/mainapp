﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ProductMvvm.ViewModels
{
    //Class for the GUI to display and modify products.
    //All product properties the GUI can touch are strings.
    //A single integer property, ProductId, is for database use only.
    //It is assigned by the DB when it creates a new product.  It is used
    //to identify a product and must not be modified by the GUI.
    public class DisplayFlags
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private bool checkoutVisible;
        public bool CheckoutVisible
        {
            get { return checkoutVisible; }
            set { checkoutVisible = value; OnPropertyChanged(new PropertyChangedEventArgs("CheckoutVisible")); }
        }

        private bool checkinVisible;
        public bool CheckinVisible
        {
            get { return checkinVisible; }
            set { checkinVisible = value; OnPropertyChanged(new PropertyChangedEventArgs("CheckinVisible")); }
        }

        private bool disabledVisible;
        public bool DisabledVisible
        {
            get { return disabledVisible; }
            set { disabledVisible = value; OnPropertyChanged(new PropertyChangedEventArgs("DisabledVisible")); }
        }

        private bool activateVisible;
        public bool ActivateVisible
        {
            get { return activateVisible; }
            set { activateVisible = value; OnPropertyChanged(new PropertyChangedEventArgs("ActivateVisible")); }
        }

        private bool checkoutCodeVisible;
        public bool CheckoutCodeVisible
        {
            get { return checkoutCodeVisible; }
            set { checkoutCodeVisible = value; OnPropertyChanged(new PropertyChangedEventArgs("CheckoutCodeVisible")); }
        }

        public DisplayFlags()
        {
        }

        public DisplayFlags(bool checkoutVisible, bool checkinVisible, bool disabledVisible, bool activateVisible)
        {
            CheckoutVisible = checkoutVisible;
            CheckinVisible = checkinVisible;
            DisabledVisible = disabledVisible;
            ActivateVisible = activateVisible;
        }

    } //class Product

}
