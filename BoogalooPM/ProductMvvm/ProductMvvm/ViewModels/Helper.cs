﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace ProductMvvm.ViewModels
{
    public class MyObservableCollectionReservation<Reservation> : ObservableCollection<Reservation>
    {
        public void UpdateCollection()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                                NotifyCollectionChangedAction.Reset));
        }


        public void ReplaceItem(int index, Reservation item)
        {
            base.SetItem(index, item);
        }

    } // class MyObservableCollection

    //public static class HelperMethods
    //{
    //    public static DateTime GetDate()
    //    {
    //        return new DateTime(2015, 2, 22);
    //    }

    //    public static DateTime GetDateTime()
    //    {
    //        DateTime date= new DateTime(2015, 2, 22);
    //        DateTime time = DateTime.Now;
    //        return date.AddHours(time.Hour).AddMinutes(time.Minute).AddSeconds(time.Second);
    //    }
    //}

}
