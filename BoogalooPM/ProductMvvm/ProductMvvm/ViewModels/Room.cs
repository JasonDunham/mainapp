﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ProductMvvm.ViewModels
{
    //Class for the GUI to display and modify products.
    //All product properties the GUI can touch are strings.
    //A single integer property, ProductId, is for database use only.
    //It is assigned by the DB when it creates a new product.  It is used
    //to identify a product and must not be modified by the GUI.
    public class Room
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        //For DB use only!
        private int _roomId;
        public int _RoomId { get { return _roomId; } }

        private int hostelId;
        public int HostelId
        {
            get { return hostelId; }
            set { hostelId = value; OnPropertyChanged(new PropertyChangedEventArgs("HostelId"));
                }
        }

        private string roomName;
        public string RoomName
        {
            get { return roomName; }
            set { roomName = value; OnPropertyChanged(new PropertyChangedEventArgs("RoomName"));
                }
        }


        public Room()
        {
        }

        public Room(int roomId, int hostelId, string roomName)
        {
            this._roomId = roomId;
            HostelId = hostelId;
            RoomName = roomName;
        }

        public void CopyRoom(Room p)
        {
            this._roomId = p._roomId;
            this.HostelId = p.HostelId;
            this.RoomName = p.RoomName;
        }

        //Creating a new product in the DB assigns the ProductId
        //Update the ProductId from the value in the corresponding SqlProduct
        public void ProductAdded2DB(SqlRoom sqlRoom)
        {
            this._roomId = sqlRoom.RoomId;
        }

    } //class Product



    //Communiction to/from SQL uses this class for product
    //It has a decimal, not string, definition for UnitCost
    //Consversion routines SqlProduct <--> Product provided
    public class SqlRoom
    {
        public int RoomId { get; set; }
        public int HostelId {get; set;}
        public string RoomName {get; set;}

        public SqlRoom() { }

        public SqlRoom(int roomId, int hostelId, string roomName)
        {
            RoomId = roomId;
            HostelId = hostelId;
            RoomName = roomName;
        }

        public SqlRoom(Room p)
        {
            RoomId = p._RoomId;
            HostelId = p.HostelId;
            RoomName = p.RoomName;
        }

        public Room SqlRoom2Room()
        {
            return new Room(RoomId, HostelId, RoomName);
        } //SqlProduct2Product()
    }

}
