﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ProductMvvm.ViewModels
{
    //Class for the GUI to display and modify products.
    //All product properties the GUI can touch are strings.
    //A single integer property, ProductId, is for database use only.
    //It is assigned by the DB when it creates a new product.  It is used
    //to identify a product and must not be modified by the GUI.
    public class User
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        //For DB use only!
        private int _userId;
        public int _UserId { get { return _userId; } }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; OnPropertyChanged(new PropertyChangedEventArgs("FirstName"));
                }
        }


        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; OnPropertyChanged(new PropertyChangedEventArgs("LastName")); }
        }

        private string emailAddress;
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; OnPropertyChanged(new PropertyChangedEventArgs("EmailAddress")); }
        }

        private int checkinCode;
        public int CheckinCode
        {
            get { return checkinCode; }
            set { checkinCode = value; OnPropertyChanged(new PropertyChangedEventArgs("CheckinCode")); }
        }

        public User()
        {
        }

        public User(int userId, string firstName, string lastName,
                       string emailAddress, int checkinCode)
        {
            this._userId = userId;
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
            CheckinCode = checkinCode;
        }

        public void CopyUser(User p)
        {
            this._userId = p._UserId;
            this.FirstName = p.FirstName;
            this.LastName = p.LastName;
            this.EmailAddress = p.EmailAddress;
            this.CheckinCode = p.CheckinCode;
        }

        //Creating a new product in the DB assigns the ProductId
        //Update the ProductId from the value in the corresponding SqlProduct
        public void UserAdded2DB(SqlUser sqlUser)
        {
            this._userId = sqlUser.UserId;
        }

    } //class Product



    //Communiction to/from SQL uses this class for product
    //It has a decimal, not string, definition for UnitCost
    //Consversion routines SqlProduct <--> Product provided
    public class SqlUser
    {
        public int UserId { get; set; }
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string EmailAddress {get; set;}
        public int CheckinCode { get; set; }

        public SqlUser() { }

        public SqlUser(int userId, string firstName, string lastName,
                       string emailAddress, int checkinCode)
        {
            UserId = userId;
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
            CheckinCode = checkinCode;
        }

        public SqlUser(User p)
        {
            UserId = p._UserId;
            FirstName = p.FirstName;
            LastName = p.LastName;
            EmailAddress = p.EmailAddress;
            CheckinCode = p.CheckinCode;
        }

        public User SqlUser2User()
        {
            return new User(UserId, FirstName, LastName, EmailAddress, CheckinCode);
        } //SqlProduct2Product()
    }

}
