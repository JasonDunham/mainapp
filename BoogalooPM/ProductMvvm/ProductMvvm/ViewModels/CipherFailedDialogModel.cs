﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmFoundation.Wpf;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;



namespace ProductMvvm.ViewModels
{
    public class CipherFailedDialogModel : INotifyPropertyChanged
    {
        private bool isSelected = false;

        private void CheckoutVisible()
        {
            ClearUserDisplay();
            DisplayFlags.CheckinVisible = false;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        private void CheckinVisible()
        {
            ClearUserDisplay();
            DisplayFlags.CheckinVisible = true;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        private void ActivateVisible()
        {
            ClearUserDisplay();
            DisplayFlags.CheckinVisible = false;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
        //data checks and status indicators done in another class
        private readonly ProductDisplayModelStatus stat = new ProductDisplayModelStatus();
        public ProductDisplayModelStatus Stat { get { return stat; } }

        private User displayedUser = new User();
        public User DisplayedUser
        {
            get { return displayedUser; }
            set { displayedUser = value; OnPropertyChanged(new PropertyChangedEventArgs("DisplayedUser")); }
        }

        private DisplayFlags displayFlags = new DisplayFlags();
        public DisplayFlags DisplayFlags
        {
            get { return displayFlags; }
            set { displayFlags = value; OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags")); }
        }

        private RelayCommand checkoutVisibleCommand;
        public ICommand CheckoutVisibleCommand
        {
            get { return checkoutVisibleCommand ?? (checkoutVisibleCommand = new RelayCommand(() => CheckoutVisible())); }
        }

        private RelayCommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand ?? (clearCommand = new RelayCommand(() => ClearUserDisplay()/*, ()=>isSelected*/)); }
        }

        private void ClearUserDisplay()
        {
            isSelected = false;
            stat.NoError();
            DisplayedUser = new User();
            App.Messenger.NotifyColleagues("UserCleared");
        } //ClearProductDisplay()


        private RelayCommand checkinCommand;
        public ICommand CheckinCommand
        {
            get { return checkinCommand ?? (checkinCommand = new RelayCommand(() => CheckinBed(), () => !isSelected)); }
        }

        private void CheckinBed()
        {
            App.Messenger.NotifyColleagues("CheckinBed", displayedUser);
            DisplayFlags.CheckinVisible = false;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        private RelayCommand continueCommand;
        public ICommand ContinueCommand
        {
            get { return continueCommand ?? (continueCommand = new RelayCommand(() => Continue(), () => !isSelected)); }
        }

        private void Continue()
        {
            DisplayFlags.CheckinVisible = false;
            App.Messenger.NotifyColleagues("ShowCheckinDialog");
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        public CipherFailedDialogModel()
        { 
            Messenger messenger = App.Messenger;
            messenger.Register("CipherFailedDialog", (Action)(() => CheckinVisible()));
            messenger.Register("SetStatus", (Action<String>)(param => stat.Status = param));
        } //ctor

        public void ProcessUser(User p)
        {
            if (p == null) { /*DisplayedProduct = null;*/  isSelected = false;  return; }
            User temp = new User();
            temp.CopyUser(p);
            DisplayedUser = temp;
            isSelected = true;
            stat.NoError();
        } // ProcessProduct()
    }
}
