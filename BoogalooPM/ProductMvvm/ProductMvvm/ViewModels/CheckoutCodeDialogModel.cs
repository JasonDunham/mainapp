﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmFoundation.Wpf;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;



namespace ProductMvvm.ViewModels
{
    public class CheckoutCodeDialogModel : INotifyPropertyChanged
    {
        private bool isSelected = false;

        private void CheckoutCodeVisible(string code)
        {
            ClearUserDisplay();
            CheckoutCodeValue = code;
            DisplayFlags.CheckoutCodeVisible = true;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
        //data checks and status indicators done in another class
        private readonly ProductDisplayModelStatus stat = new ProductDisplayModelStatus();
        public ProductDisplayModelStatus Stat { get { return stat; } }

        private User displayedUser = new User();
        public User DisplayedUser
        {
            get { return displayedUser; }
            set { displayedUser = value; OnPropertyChanged(new PropertyChangedEventArgs("DisplayedUser")); }
        }

        private string checkoutCode = "627125";
        public string CheckoutCodeValue
        {
            get { return checkoutCode; }
            set { checkoutCode = value; OnPropertyChanged(new PropertyChangedEventArgs("CheckoutCodeValue")); }
        }

        private DisplayFlags displayFlags = new DisplayFlags();
        public DisplayFlags DisplayFlags
        {
            get { return displayFlags; }
            set { displayFlags = value; OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags")); }
        }

        private RelayCommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand ?? (clearCommand = new RelayCommand(() => ClearUserDisplay()/*, ()=>isSelected*/)); }
        }

        private void ClearUserDisplay()
        {
            isSelected = false;
            stat.NoError();
            DisplayedUser = new User();
            App.Messenger.NotifyColleagues("UserCleared");
        } //ClearProductDisplay()


        private RelayCommand checkoutCodeCommand;
        public ICommand CheckoutCodeCommand
        {
            get { return checkoutCodeCommand ?? (checkoutCodeCommand = new RelayCommand(() => CheckoutCode(), () => !isSelected)); }
        }

        private void CheckoutCode()
        {
            DisplayFlags.CheckoutCodeVisible = true;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        private RelayCommand continueCommand;
        public ICommand ContinueCommand
        {
            get { return continueCommand ?? (continueCommand = new RelayCommand(() => Continue(), () => !isSelected)); }
        }

        private void Continue()
        {
            DisplayFlags.CheckoutCodeVisible = false;
            OnPropertyChanged(new PropertyChangedEventArgs("DisplayFlags"));
        }

        public CheckoutCodeDialogModel()
        { 
            Messenger messenger = App.Messenger;
            messenger.Register("ShowCheckoutCodeDialog", (Action<string>)(param => CheckoutCodeVisible(param)));
            messenger.Register("SetStatus", (Action<String>)(param => stat.Status = param));
        } //ctor

        public void ProcessUser(User p)
        {
            if (p == null) { /*DisplayedProduct = null;*/  isSelected = false;  return; }
            User temp = new User();
            temp.CopyUser(p);
            DisplayedUser = temp;
            isSelected = true;
            stat.NoError();
        } // ProcessProduct()
    }
}
