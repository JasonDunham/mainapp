﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MvvmFoundation.Wpf;

namespace ProductMvvm.ViewModels
{
    public class DisabledBunkSelectionModel : INotifyPropertyChanged
    {

        public DisabledBunkSelectionModel()
        {
            dataItems = new MyObservableCollectionBunk<Bed>();
            DataItems = App.BoogalooDB.GetDisabledBeds();
            listBoxCommand = new RelayCommand(() => SelectionHasChanged());
            App.Messenger.Register("BunkCleared", (Action)(() => SelectedBunk = null));
            App.Messenger.Register("GetBeds", (Action)(() => GetDisabledBunks()));
            App.Messenger.Register("RoomSelectionChanged", (Action<Room>)(param => ChangeRoomSelection(param)));
            App.Messenger.Register("UpdateBunk", (Action<Bed>)(param => UpdateBunk(param)));
            App.Messenger.Register("DeleteBunk", (Action)(() => DeleteBunk()));
            App.Messenger.Register("AddBunk", (Action<Bed>)(param => AddBunk(param)));
        }

        private Room activeRoom = default(Room);

        private void ChangeRoomSelection(Room room)
        {
            activeRoom = room;
            GetDisabledBunks();
        }

        private void GetDisabledBunks()
        {
            if (activeRoom == default(Room))
            {
                DataItems = App.BoogalooDB.GetDisabledBeds();
            }
            else
            {
                DataItems = App.BoogalooDB.GetDisabledBedsByRoomId(activeRoom);
            }
            if (App.StoreDB.hasError)
                App.Messenger.NotifyColleagues("SetStatus", App.StoreDB.errorMessage);
        }


        private void AddBunk(Bed p)
        {
            dataItems.Add(p);
        }


        private void UpdateBunk(Bed p)
        {
            int index = dataItems.IndexOf(selectedBunk);
            dataItems.ReplaceItem(index, p);
            SelectedBunk = p;
        }


        private void DeleteBunk()
        {
            dataItems.Remove(selectedBunk);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private MyObservableCollectionBunk<Bed> dataItems;
        public MyObservableCollectionBunk<Bed> DataItems
        {
            get { return dataItems; }
            //If dataItems replaced by new collection, WPF must be told
            set { dataItems = value; OnPropertyChanged(new PropertyChangedEventArgs("DataItems")); }
        }

        private Bed selectedBunk;
        public Bed SelectedBunk
        {
            get { return selectedBunk; }
            set { selectedBunk = value; OnPropertyChanged(new PropertyChangedEventArgs("SelectedBunk")); }
        }

        private RelayCommand listBoxCommand;
        public ICommand ListBoxCommand
        {
            get { return listBoxCommand; }
        }

        private void SelectionHasChanged()
        {
            Messenger messenger = App.Messenger;
            messenger.NotifyColleagues("DisabledBunkSelectionChanged", selectedBunk);
        }
    }//class ProductSelectionModel


}
