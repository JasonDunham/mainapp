﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ProductMvvm.ViewModels
{
    //Class for the GUI to display and modify products.
    //All product properties the GUI can touch are strings.
    //A single integer property, ProductId, is for database use only.
    //It is assigned by the DB when it creates a new product.  It is used
    //to identify a product and must not be modified by the GUI.
    public class Bed
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        //For DB use only!
        private int _bedId;
        public int _BedId { get { return _bedId; } }

        private int hostelId;
        public int HostelId
        {
            get { return hostelId; }
            set { hostelId = value; OnPropertyChanged(new PropertyChangedEventArgs("HostelId"));
                }
        }

        private string bedName;
        public string BedName
        {
            get { return bedName; }
            set { bedName = value; OnPropertyChanged(new PropertyChangedEventArgs("BedName"));
                }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value; OnPropertyChanged(new PropertyChangedEventArgs("BedNotes"));
            }
        }

        private int rate;
        public int Rate
        {
            get { return rate; }
            set
            {
                rate = value; OnPropertyChanged(new PropertyChangedEventArgs("BedRate"));
            }
        }

        private int status;
        public int Status
        {
            get { return status; }
            set
            {
                status = value; OnPropertyChanged(new PropertyChangedEventArgs("BedStatus"));
            }
        }

        private int roomId;
        public int RoomId
        {
            get { return roomId; }
            set
            {
                roomId = value; OnPropertyChanged(new PropertyChangedEventArgs("RoomId"));
            }
        }

        private Reservation mostRecentReserve;
        public Reservation MostRecentReserve
        {
            get { return mostRecentReserve; }
            set
            {
                mostRecentReserve = value; OnPropertyChanged(new PropertyChangedEventArgs("MostRecentReserve"));
            }
        }


        public Bed()
        {
        }

        public Bed(int bedId, int roomId, int hostelId, string bedName, int status, int rate, string notes, Reservation reserve)
        {
            this._bedId = bedId;
            HostelId = hostelId;
            BedName = bedName;
            RoomId = roomId;
            Status = status;
            Rate = rate;
            Notes = notes;
            MostRecentReserve = reserve;

        }

        public void CopyBed(Bed p)
        {
            this._bedId = p._bedId;
            this.HostelId = p.HostelId;
            this.BedName = p.BedName;
            this.Status = p.Status;
            this.Rate = p.Rate;
            this.RoomId = p.RoomId;
            this.Notes = p.Notes;
            this.MostRecentReserve = p.MostRecentReserve;
        }

        //Creating a new product in the DB assigns the ProductId
        //Update the ProductId from the value in the corresponding SqlProduct
        public void ProductAdded2DB(SqlBed sqlBed)
        {
            this._bedId = sqlBed.BedId;
        }

    } //class Product



    //Communiction to/from SQL uses this class for product
    //It has a decimal, not string, definition for UnitCost
    //Consversion routines SqlProduct <--> Product provided
    public class SqlBed
    {
        public int BedId { get; set; }
        public int HostelId {get; set;}
        public string BedName {get; set;}
        public int Status { get; set; }
        public int Rate { get; set; }
        public int RoomId { get; set; }
        public string Notes { get; set; }
        public Reservation Reserve { get; set; }

        public SqlBed() { }

        public SqlBed(int roomId, int hostelId, string bedName, int status, int rate, int room, string notes, Reservation reserve)
        {
            BedId = roomId;
            HostelId = hostelId;
            BedName = bedName;
            Status = status;
            Rate = rate;
            RoomId = room;
            Notes = notes;
            Reserve = reserve;
        }

        public SqlBed(Bed p)
        {
            BedId = p._BedId;
            HostelId = p.HostelId;
            BedName = p.BedName;
            RoomId = p.RoomId;
            Status = p.Status;
            Rate = p.Rate;
            Notes = p.Notes;
            Reserve = p.MostRecentReserve;
        }

        public Bed SqlBed2Bed()
        {
            return new Bed(BedId, RoomId, HostelId, BedName, Status, Rate, Notes, Reserve);
        } //SqlProduct2Product()
    }

}
