﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProductMvvm.Views
{
    /// <summary>
    /// Interaction logic for ProductSelectionView.xaml
    /// </summary>
    public partial class DisabledBunkSelectionView : UserControl
    {
        public DisabledBunkSelectionView()
        {
            InitializeComponent();
        }

        private void Rectangle_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            if (out_of_order_listbox.Visibility == Visibility.Visible)
                out_of_order_listbox.Visibility = Visibility.Collapsed;
            else
                out_of_order_listbox.Visibility = Visibility.Visible;
        }

        private void NewMethod(object sender, RoutedEventArgs e)
        {
            out_of_order_listbox.UnselectAll();
        }


    }
}
