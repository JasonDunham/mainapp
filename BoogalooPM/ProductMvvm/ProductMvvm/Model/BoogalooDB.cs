﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using ProductMvvm;
using ProductMvvm.ViewModels;

namespace ProductMvvm.Model
{
    public class BoogalooDB
    {
        public bool hasError = false;
        public string errorMessage;
        /*
                private string conString = Properties.Settings.Default.StoreDBConnString;
         
                public MyObservableCollection<Product> GetProducts()
                {
                    hasError = false;
                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("GetProducts", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    MyObservableCollection<Product> products = new MyObservableCollection<Product>();
                    try
                    {
                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            //create a Product object for the row
                            SqlProduct sqlProduct = new SqlProduct(
                                (int) reader["ProductId"],
                                (string)reader["ModelNumber"],
                                (string)reader["ModelName"],
                                (decimal)reader["UnitCost"],
                                GetStringOrNull(reader, "Description"),
                                (String)reader["CategoryName"]);
                            products.Add(sqlProduct.SqlProduct2Product());
                        } //while
                    } //try
                    catch (SqlException ex)
                    {
                        errorMessage = "GetProducts SQL error, " + ex.Message;
                        hasError = true;
                    }
                    catch (Exception ex)
                    {
                        errorMessage = "GetProducts error, " + ex.Message;
                        hasError = true;
                    }
                    finally
                    {
                        con.Close();
                    }
                    return products;
                } //GetProducts()
        */
        public MyObservableCollection2<ViewModels.Room> GetRooms()
        {
            hasError = false;
            MyObservableCollection2<ViewModels.Room> rooms = new MyObservableCollection2<ViewModels.Room>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                var query = from q in dc.Rooms
                    select new SqlRoom{
                        RoomId = q.RoomId, HostelId = q.HostelId,
                        RoomName=q.RoomName
                    };
                foreach (SqlRoom sp in query)
                    rooms.Add(sp.SqlRoom2Room());
            } //try
            catch(Exception ex)
            {
                errorMessage = "GetRooms() error, " + ex.Message;
                hasError = true;
            }
            return rooms;
        } //GetProducts()


        public MyObservableCollectionBunk<ViewModels.Bed> GetBeds()
        {
            hasError = false;
            MyObservableCollectionBunk<ViewModels.Bed> beds = new MyObservableCollectionBunk<ViewModels.Bed>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                var query = from q in dc.Beds
                            where q.Status == 1
                            select new SqlBed
                            {
                                BedId = q.BedId,
                                RoomId = q.RoomId,
                                HostelId = q.HostelId,
                                BedName = q.BedName,
                                Status = q.Status,
                                Rate = q.RateId,
                                Notes = q.Notes
                            };

                foreach (SqlBed sp in query)
                    beds.Add(sp.SqlBed2Bed());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetBeds() error, " + ex.Message;
                hasError = true;
            }
            return beds;
        } //GetProducts()

        public MyObservableCollectionBunk<ViewModels.Bed> GetBedsByRoomId(ViewModels.Room room)
        {
            hasError = false;
            MyObservableCollectionBunk<ViewModels.Bed> beds = new MyObservableCollectionBunk<ViewModels.Bed>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                var query = from q in dc.Beds
                            where q.Status == 1 && q.RoomId == room._RoomId
                            select new SqlBed
                            {
                                BedId = q.BedId,
                                RoomId = q.RoomId,
                                HostelId = q.HostelId,
                                BedName = q.BedName,
                                Status = q.Status,
                                Rate = q.RateId,
                                Notes = q.Notes
                            };
                foreach (SqlBed sp in query)
                    beds.Add(sp.SqlBed2Bed());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetBedsByRoomId() error, " + ex.Message;
                hasError = true;
            }
            return beds;
        } //GetProducts()


        public MyObservableCollectionBunk<ViewModels.Bed> GetDisabledBedsByRoomId(ViewModels.Room room)
        {
            hasError = false;
            MyObservableCollectionBunk<ViewModels.Bed> beds = new MyObservableCollectionBunk<ViewModels.Bed>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                var query = from q in dc.Beds
                            where q.Status != 1 && q.RoomId == room._RoomId
                            select new SqlBed
                            {
                                BedId = q.BedId,
                                RoomId = q.RoomId,
                                HostelId = q.HostelId,
                                BedName = q.BedName,
                                Status = q.Status,
                                Rate = q.RateId,
                                Notes = q.Notes
                            };
                foreach (SqlBed sp in query)
                    beds.Add(sp.SqlBed2Bed());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetDisabledBeds() error, " + ex.Message;
                hasError = true;
            }
            return beds;
        }

        public MyObservableCollectionBunk<ViewModels.Bed> GetDisabledBeds()
        {
            hasError = false;
            MyObservableCollectionBunk<ViewModels.Bed> beds = new MyObservableCollectionBunk<ViewModels.Bed>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                var query = from q in dc.Beds
                            where q.Status != 1
                            select new SqlBed
                            {
                                BedId = q.BedId,
                                RoomId = q.RoomId,
                                HostelId = q.HostelId,
                                BedName = q.BedName,
                                Status = q.Status,
                                Rate = q.RateId,
                                Notes = q.Notes
                            };
                foreach (SqlBed sp in query)
                    beds.Add(sp.SqlBed2Bed());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetDisabledBeds() error, " + ex.Message;
                hasError = true;
            }
            return beds;
        } //GetProducts()

        public MyObservableCollectionReservation<ViewModels.Reservation> GetReservations()
        {
            hasError = false;
            MyObservableCollectionReservation<ViewModels.Reservation> reservations = new MyObservableCollectionReservation<ViewModels.Reservation>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                var query = from q in dc.Reservations
                            select new SqlReservation
                            {
                                BedId = q.BedId,
                                ReservationId = q.ReservationId,
                                StartDate = q.StartDate,
                                EndDate = q.EndDate,
                                Recalled = q.Recalled,
                                EmployeeId = q.EmployeeId,
                                Disabled = q.Disabled
                            };
                foreach (SqlReservation sp in query)
                    reservations.Add(sp.SqlReservation2Reservation());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetReservations() error, " + ex.Message;
                hasError = true;
            }
            return reservations;
        } //GetProducts()

        public MyObservableCollectionReservation<ViewModels.Reservation> GetCurrentReservationsByBedId(int bedId)
        {
            hasError = false;
            MyObservableCollectionReservation<ViewModels.Reservation> reservations = new MyObservableCollectionReservation<ViewModels.Reservation>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                DateTime date = App.TimeManager.GetDate();
                var query = from q in dc.Reservations
                            where q.BedId == bedId && q.EndDate == null && q.Recalled == null
                            select new SqlReservation
                            {
                                BedId = q.BedId,
                                ReservationId = q.ReservationId,
                                StartDate = q.StartDate,
                                EndDate = q.EndDate,
                                Recalled = q.Recalled,
                                EmployeeId = q.EmployeeId,
                                Disabled = q.Disabled
                            };
                foreach (SqlReservation sp in query)
                    reservations.Add(sp.SqlReservation2Reservation());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetReservations() error, " + ex.Message;
                hasError = true;
            }
            return reservations;
        } //GetProducts()

        public ViewModels.Reservation GetMostRecentReservationsByBedId(int bedId)
        {
            hasError = false;
            ViewModels.Reservation reservation = new ViewModels.Reservation();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                DateTime date = App.TimeManager.GetDate();
                var query = from q in dc.Reservations
                            where q.BedId == bedId
                            orderby q.StartDate descending
                            select new SqlReservation
                            {
                                BedId = q.BedId,
                                ReservationId = q.ReservationId,
                                StartDate = q.StartDate,
                                EndDate = q.EndDate,
                                Recalled = q.Recalled,
                                EmployeeId = q.EmployeeId,
                                Disabled = q.Disabled
                            };
                if (query.FirstOrDefault() == default(SqlReservation))
                {
                    reservation = default(ViewModels.Reservation);
                }
                else
                {
                    reservation = query.First().SqlReservation2Reservation();
                }
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetReservations() error, " + ex.Message;
                hasError = true;
            }
            return reservation;
        } //GetProducts()

        public MyObservableCollectionReservation<ViewModels.Reservation> GetAllCurrentReservations()
        {
            hasError = false;
            MyObservableCollectionReservation<ViewModels.Reservation> reservations = new MyObservableCollectionReservation<ViewModels.Reservation>();
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                DateTime date = App.TimeManager.GetDate();
                var query = from q in dc.Reservations
                            where q.EndDate == null && q.Recalled == null
                            select new SqlReservation
                            {
                                BedId = q.BedId,
                                ReservationId = q.ReservationId,
                                StartDate = q.StartDate,
                                EndDate = q.EndDate,
                                Recalled = q.Recalled,
                                EmployeeId = q.EmployeeId,
                                Disabled = q.Disabled
                            };
                foreach (SqlReservation sp in query)
                    reservations.Add(sp.SqlReservation2Reservation());
            } //try
            catch (Exception ex)
            {
                errorMessage = "GetTodayReservations() error, " + ex.Message;
                hasError = true;
            }
            return reservations;
        } //GetProducts()



/*
        private string GetStringOrNull(SqlDataReader reader, string columnName)
        {
            return reader.IsDBNull(reader.GetOrdinal(columnName)) ? "" : (string)reader[columnName]; 
        }
*/

        /*
        private const int prodStringLen = 50;
        public bool UpdateProduct(Product displayP)
        {
            SqlProduct p = new SqlProduct( displayP);
            hasError = false;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("UpdateProduct", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ProductId", SqlDbType.Int, 4);
            cmd.Parameters["@ProductId"].Value = p.ProductId;
            cmd.Parameters.Add("@ModelNumber", SqlDbType.VarChar, prodStringLen);
            cmd.Parameters["@ModelNumber"].Value = p.ModelNumber;
            cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, prodStringLen);
            cmd.Parameters["@ModelName"].Value = p.ModelName;
            cmd.Parameters.Add("@UnitCost", SqlDbType.Decimal);
            cmd.Parameters["@UnitCost"].Value = p.UnitCost;
            cmd.Parameters.Add("@Description", SqlDbType.VarChar, 200);
            cmd.Parameters["@Description"].Value = p.Description;
            cmd.Parameters.Add("@CategoryName", SqlDbType.VarChar, prodStringLen);
            cmd.Parameters["@CategoryName"].Value = p.CategoryName;
            int rows = 0;
            try
            {
                con.Open();
                rows = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                errorMessage = "Update SQL error, " + ex.Message;
                hasError = true;
            }
            catch (Exception ex)
            {
                errorMessage = "Update error, " + ex.Message;
                hasError = true;
            }
            finally
            {
                con.Close();
            }
            return (!hasError);
        } //UpdateProduct()
*/
        public bool UpdateProduct(Product displayP)
        {
            try
            {
                SqlProduct p = new SqlProduct(displayP);
                LinqDataContext dc = new LinqDataContext();
                dc.UpdateProduct(p.ProductId, p.CategoryName, p.ModelNumber, p.ModelName, p.UnitCost, p.Description);
            }
            catch (Exception ex)
            {
                errorMessage = "Update error, " + ex.Message;
                hasError = true;
            }
            return (!hasError);
        } //UpdateProduct()


        public bool UpdateBed(ViewModels.Bed displayP)
        {
            try
            {
                SqlBed p = new SqlBed(displayP);
                DataClasses1DataContext dc = new DataClasses1DataContext();
                dc.UpdateBed(p.BedId, p.RoomId, p.HostelId, p.BedName, p.Status, p.Rate,p.Notes);
            }
            catch (Exception ex)
            {
                errorMessage = "Update error, " + ex.Message;
                hasError = true;
            }
            return (!hasError);
        } //UpdateProduct()

        public bool UpdateReservation(ViewModels.Reservation displayP)
        {
            try
            {
                SqlReservation p = new SqlReservation(displayP);
                DataClasses1DataContext dc = new DataClasses1DataContext();
                dc.UpdateReservation(p.ReservationId, p.BedId, p.StartDate, p.EndDate, p.Recalled, p.EmployeeId, p.Disabled);
            }
            catch (Exception ex)
            {
                errorMessage = "Update error, " + ex.Message;
                hasError = true;
            }
            return (!hasError);
        } //UpdateProduct()




/*
        public bool DeleteProduct(int productId)
        {
            hasError = false;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("DeleteProduct", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ProductId", SqlDbType.Int, 4);
            cmd.Parameters["@ProductId"].Value = productId;
            try
            {
                con.Open();
                int rows = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                errorMessage = "DELETE SQL error, " + ex.Message;
                hasError = true;
            }
            catch (Exception ex)
            {
                errorMessage = "DELETE error, " + ex.Message;
                hasError = true;
            }
            finally
            {
                con.Close();
            }
            return !hasError;
        }// DeleteProduct()
*/
        public bool DeleteProduct(int productId)
        {
            hasError = false;
            try
            {
                LinqDataContext dc = new LinqDataContext();
                dc.DeleteProduct(productId);
            }
            catch (Exception ex)
            {
                errorMessage = "Delete error, " + ex.Message;
                hasError = true;
            }
            return !hasError;
        }// DeleteProduct()


        public bool DeleteBed(int bedId)
        {
            //hasError = false;
            //try
            //{
            //    LinqDataContext dc = new LinqDataContext();
            //    dc.DeleteProduct(productId);
            //}
            //catch (Exception ex)
            //{
            //    errorMessage = "Delete error, " + ex.Message;
            //    hasError = true;
            //}
            //return !hasError;
            return true;
        }// DeleteProduct()

        public bool DeleteReservation(int reservationId)
        {
            hasError = false;
            try
            {
                DataClasses1DataContext dc = new DataClasses1DataContext();
                dc.DeleteReservation(reservationId);
            }
            catch (Exception ex)
            {
                errorMessage = "Delete error, " + ex.Message;
                hasError = true;
            }
            return !hasError;
        }// DeleteProduct()


/*
        public bool AddProduct(Product displayP)
        {
            SqlProduct p = new SqlProduct(displayP);
            hasError = false;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("AddProduct", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ModelNumber", SqlDbType.VarChar, prodStringLen);
            cmd.Parameters["@ModelNumber"].Value = p.ModelNumber;
            cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, prodStringLen);
            cmd.Parameters["@ModelName"].Value = p.ModelName;
            cmd.Parameters.Add("@UnitCost", SqlDbType.Decimal);
            cmd.Parameters["@UnitCost"].Value = p.UnitCost;
            cmd.Parameters.Add("@Description", SqlDbType.VarChar, 200);
            cmd.Parameters["@Description"].Value = p.Description;
            if (p.Description==null)  cmd.Parameters["@Description"].Value = DBNull.Value;
            cmd.Parameters.Add("@CategoryName", SqlDbType.VarChar, prodStringLen);
            cmd.Parameters["@CategoryName"].Value = p.CategoryName;
            cmd.Parameters.Add("@ProductId", SqlDbType.Int, 4);
            cmd.Parameters["@ProductId"].Value = p.ProductId;
            cmd.Parameters["@ProductId"].Direction = ParameterDirection.Output;
            try
            {
                con.Open();
                int rows = cmd.ExecuteNonQuery();                       //create the new product in DB
                p.ProductId = (int)cmd.Parameters["@ProductId"].Value;  //set the returned ProductId in the SqlProduct object
                displayP.ProductAdded2DB(p);                            //update corresponding Product ProductId using SqlProduct
            }
            catch (SqlException ex)
            {
                errorMessage = "Add SQL error, " + ex.Message;
                hasError = true;
            }
            catch (Exception ex)
            {
                errorMessage = "ADD error, " + ex.Message;
                hasError = true;
            }
            finally
            {
                con.Close();
            }
            return !hasError;
        } //AddProduct()
 */ 
        public bool AddProduct(Product displayP)
        {
            hasError = false;
            try
            {
                SqlProduct p = new SqlProduct(displayP);
                LinqDataContext dc = new LinqDataContext();
                int? newProductId = 0;
                dc.AddProduct(p.CategoryName, p.ModelNumber, p.ModelName, p.UnitCost, p.Description, ref newProductId);
                p.ProductId = (int)newProductId;
                displayP.ProductAdded2DB(p);    //update corresponding Product ProductId using SqlProduct
            }
            catch (Exception ex)
            {
                errorMessage = "Add error, " + ex.Message;
                hasError = true;
            }
            return !hasError;
        } //AddProduct()

        public bool AddReservation(ViewModels.Reservation displayP)
        {
            hasError = false;
            try
            {
                SqlReservation p = new SqlReservation(displayP);
                DataClasses1DataContext dc = new DataClasses1DataContext();
                int? newReservationId = 0;
                dc.AddReservation(p.BedId, p.StartDate, p.EndDate, p.Recalled, p.EmployeeId, p.Disabled, ref newReservationId);
                p.ReservationId = (int)newReservationId;
                displayP.ReservationAdded2DB(p);    //update corresponding Product ProductId using SqlProduct
            }
            catch (Exception ex)
            {
                errorMessage = "Add error, " + ex.Message;
                hasError = true;
            }
            return !hasError;
        } //AddProduct()



        public bool AddBed(ViewModels.Bed displayP)
        {
            //hasError = false;
            //try
            //{
            //    SqlProduct p = new SqlProduct(displayP);
            //    LinqDataContext dc = new LinqDataContext();
            //    int? newProductId = 0;
            //    dc.AddProduct(p.CategoryName, p.ModelNumber, p.ModelName, p.UnitCost, p.Description, ref newProductId);
            //    p.ProductId = (int)newProductId;
            //    displayP.ProductAdded2DB(p);    //update corresponding Product ProductId using SqlProduct
            //}
            //catch (Exception ex)
            //{
            //    errorMessage = "Add error, " + ex.Message;
            //    hasError = true;
            //}
            //return !hasError;

            return true;
        } //AddProduct()



    } //class StoreDB
}
