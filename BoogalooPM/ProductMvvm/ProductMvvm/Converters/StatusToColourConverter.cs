﻿using ProductMvvm.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ProductMvvm.Converters
{
    [ValueConversion(typeof(Bed_Status_Enum), typeof(Brush))]
    class StatusToColourConverter : IValueConverter
    {
            public object Convert(object value, Type targetType,
                object parameter, CultureInfo culture)
            {
                Bed_Status_Enum status = (Bed_Status_Enum)value;

                SolidColorBrush myBrush = new SolidColorBrush();

                switch (status)
                {
                    case Bed_Status_Enum.Available:
                        { myBrush.Color = Color.FromArgb(255,157,238,172);
                        return myBrush; 
                        }
                    case Bed_Status_Enum.Booked:
                        {
                            myBrush.Color = Color.FromArgb(255, 167, 158, 255);
                            return myBrush;
                        }
                    case Bed_Status_Enum.Unavailable:
                        {
                            myBrush.Color = Color.FromArgb(255, 251, 255, 158);
                            return myBrush;
                        }
                    case Bed_Status_Enum.Disabled:
                        {
                            myBrush.Color = Color.FromArgb(255, 255, 158, 158);
                            return myBrush;
                        }
                }
                return new SolidColorBrush(Colors.Transparent);


            }

            public object ConvertBack(object value, Type targetType,
                object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
    }
}
